package org.adorozhkin.transfer.stressTest;

import com.despegar.http.client.HttpResponse;
import com.despegar.http.client.PostMethod;
import com.despegar.sparkjava.test.SparkServer;
import com.google.gson.Gson;
import org.adorozhkin.transfer.rest.impl.RestController;
import org.adorozhkin.transfer.rest.impl.TransferMoneyRequest;
import org.adorozhkin.transfer.service.impl.ServiceFactory;
import org.junit.ClassRule;
import spark.servlet.SparkApplication;

import static org.junit.Assert.assertEquals;

public class RestControllerStressTest extends AbstractStressTest {
    private static final int PORT = 56789;
    private Gson gson = new Gson();

    public static class TestApplication implements SparkApplication {
        private ServiceFactory factory = new ServiceFactory();

        @Override
        public void init() {
            new RestController(factory.getServiceDecorator()).init();
        }
    }

    @ClassRule
    public static SparkServer<TestApplication> testServer = new SparkServer<>(TestApplication.class, PORT);

    @Override
    protected void performTransfer(long from, long to, double amount) throws Exception {
        TransferMoneyRequest transferRequest = new TransferMoneyRequest(from, to, amount);
        PostMethod put = testServer.post("/transfer", gson.toJson(transferRequest), false);
        HttpResponse response = testServer.execute(put);
        assertEquals(200, response.code());
    }
}
