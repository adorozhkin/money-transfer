package org.adorozhkin.transfer.stressTest;

import org.adorozhkin.transfer.model.Account;
import org.adorozhkin.transfer.service.impl.AccountServiceDecorator;
import org.adorozhkin.transfer.service.impl.ServiceFactory;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

import static org.junit.Assert.assertEquals;

public abstract class AbstractStressTest {
    private static final int ACCOUNTS = 100;
    private static final int THREADS = 50;
    private final int ITERATIONS = 10_000;
    private static final double DEPOSIT = 1_000_000;

    private static final double DELTA = 0.1;
    private ExecutorService executor = Executors.newFixedThreadPool(THREADS);

    private CompletionService<Void> completionService = new ExecutorCompletionService<>(executor);
    private ServiceFactory factory = new ServiceFactory();

    AccountServiceDecorator accountService = factory.getServiceDecorator();


    @Test
    public void transferMoneyStress() throws Exception {
        List<Account> accounts = new ArrayList<>();
        for (int i = 0; i < ACCOUNTS; i++) {
            Account account = accountService.createAccount("description" + i);
            accounts.add(accountService.depositAccount(account.getAccountNumber(), DEPOSIT));
        }

        for (int i = 0; i < ITERATIONS; i++) {
            completionService.submit(() -> {
                int from = 0;
                int to = 0;
                while (from == to) {
                    from = ThreadLocalRandom.current().nextInt(ACCOUNTS);
                    to = ThreadLocalRandom.current().nextInt(ACCOUNTS);
                }

                performTransfer(accounts.get(from).getAccountNumber(), accounts.get(to).getAccountNumber(), DEPOSIT / ITERATIONS);
                return null;
            });
        }

        for (int i = 0; i < ITERATIONS; i++) {
            completionService.take().get();
        }

        double totalDeposit = 0;
        for (int i = 0; i < ACCOUNTS; i++) {
            Account account = accountService.loadAccount(accounts.get(i).getAccountNumber());
            totalDeposit += account.getAmount();
        }
        assertEquals("All transfers are correct", DEPOSIT * ACCOUNTS, totalDeposit, DELTA);
    }

    protected abstract void performTransfer(long from, long to, double amount) throws Exception;
}
