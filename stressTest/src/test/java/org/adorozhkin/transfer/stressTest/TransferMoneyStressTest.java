package org.adorozhkin.transfer.stressTest;

public class TransferMoneyStressTest extends AbstractStressTest {

    @Override
    protected void performTransfer(long from, long to, double amount) throws Exception {
        accountService.transfer(from, to, amount);
    }
}
