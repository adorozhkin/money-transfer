package org.adorozhkin.transfer.service.mock;

import org.adorozhkin.transfer.dao.AccountDao;
import org.adorozhkin.transfer.model.Account;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class AccountDaoMock implements AccountDao {
    private static final Map<Long, Account> store = new ConcurrentHashMap<>();
    private static final AtomicLong accountNumberGenerator = new AtomicLong(0);


    @Override
    public long save(Account account) {
        long accountNumber = accountNumberGenerator.incrementAndGet();
        account.setAccountNumber(accountNumber);
        store.put(accountNumber, account);
        return accountNumber;
    }

    @Override
    public void update(Account account) {
        store.put(account.getAccountNumber(), account);
    }

    @Override
    public void delete(long accountNumber) {
        store.remove(accountNumber);
    }

    @Override
    public Account load(long accountNumber, boolean forUpdate) {
        return store.get(accountNumber);
    }

    @Override
    public List<Account> loadAll() {
        return new ArrayList<>(store.values());
    }
}
