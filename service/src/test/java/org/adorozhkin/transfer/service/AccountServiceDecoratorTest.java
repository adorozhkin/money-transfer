package org.adorozhkin.transfer.service;

import org.adorozhkin.transfer.model.Account;
import org.adorozhkin.transfer.service.impl.AccountServiceDecorator;
import org.adorozhkin.transfer.service.impl.ServiceFactory;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class AccountServiceDecoratorTest {
    private static final double DELTA = 0.1;

    private ServiceFactory factory = new ServiceFactory();
    private AccountServiceDecorator accountService = factory.getServiceDecorator();

    @Test
    public void testTransferMoney() throws Exception {
        Account from = accountService.createAccount("from");
        Account to = accountService.createAccount("to");
        accountService.depositAccount(from.getAccountNumber(), 100);
        accountService.transfer(from.getAccountNumber(), to.getAccountNumber(), 10);

        Account fromLoaded = accountService.loadAccount(from.getAccountNumber());
        Account toLoaded = accountService.loadAccount(to.getAccountNumber());

        assertEquals("From account withdrawn", 90, fromLoaded.getAmount(), DELTA);
        assertEquals("From account deposited", 10, toLoaded.getAmount(), DELTA);
    }

}