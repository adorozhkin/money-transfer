package org.adorozhkin.transfer.service.impl;

import org.adorozhkin.transfer.dao.AccountDao;
import org.adorozhkin.transfer.model.Account;
import org.adorozhkin.transfer.service.AccountService;
import org.adorozhkin.transfer.service.mock.AccountDaoMock;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class AccountServiceTest {

    private AccountDao accountDao = new AccountDaoMock();
    private AccountService accountService = new AccountServiceImpl(accountDao);

    private static final String accountDescription = "description";

    private static final double DELTA = 0.1;

    private Account account;

    @Before
    public void setup() {
        account = accountService.createAccount(accountDescription);
    }

    @After
    public void tearDown() {
        accountService.deleteAccount(account);
    }

    @Test
    public void testCreateAccount() {
        assertNotNull("Account should be created", account);
        assertEquals("Account description should be set", accountDescription, account.getDescription());
        assertTrue("Account number should be set", account.getAccountNumber() != 0);

        Account loadedAccount = accountService.loadAccount(account.getAccountNumber(), false);
        assertNotNull("Account should be created", loadedAccount);
        assertNotNull("Account description should be set", loadedAccount.getDescription());
        assertTrue("Account number should be set", loadedAccount.getAccountNumber() != 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateAccountNull() {
        accountService.createAccount(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateAccountEmpty() {
        accountService.createAccount("  ");
    }

    @Test
    public void testDeleteAccount() {
        Account loadedAccount = accountService.loadAccount(account.getAccountNumber(), true);
        assertNotNull("Account created", loadedAccount);

        accountService.deleteAccount(account);
        loadedAccount = accountService.loadAccount(account.getAccountNumber(), false);
        assertNull("Account removed", loadedAccount);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeleteAccountNull() {
        accountService.deleteAccount(null);
    }

    @Test
    public void testDepositAccount() {
        double deposit = 100.99;
        account = accountService.depositAccount(account, deposit);
        assertEquals("Account is deposited", deposit, account.getAmount(), DELTA);

        account = accountService.depositAccount(account, deposit);
        assertEquals("Account is deposited", deposit * 2, account.getAmount(), DELTA);

        Account loadedAccount = accountService.loadAccount(account.getAccountNumber(), false);
        assertEquals("Account is deposited in user", deposit * 2, loadedAccount.getAmount(), DELTA);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDepositAccountNull() {
        accountService.depositAccount(null, 100);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDepositAccountNegative() {
        accountService.depositAccount(account, -100);
    }

    @Test
    public void testWithdrawAccount() {
        double deposit = 100.99;
        account = accountService.depositAccount(account, deposit);
        assertEquals("Account is deposited", deposit, account.getAmount(), DELTA);

        account = accountService.withdrawAccount(account, deposit);
        assertEquals("Account is withdrawn", 0, account.getAmount(), DELTA);

        Account loadedAccount = accountService.loadAccount(account.getAccountNumber(), false);
        assertEquals("Account is withdrawn in user", 0, loadedAccount.getAmount(), DELTA);
    }

    @Test(expected = IllegalStateException.class)
    public void testWithdrawAccountNotEnough() {
        double deposit = 100.99;
        accountService.withdrawAccount(account, deposit);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testWithdrawAccountNull() {
        accountService.withdrawAccount(null, 100);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWithdrawAccountNegative() {
        accountService.withdrawAccount(account, -100);
    }

    @Test
    public void testLoadAll() {
        tearDown();
        List<Account> accounts = accountService.loadAll();
        assertNotNull("Accounts list is not null");
        assertTrue("Accounts list is empty", accounts.isEmpty());

        accountService.createAccount(accountDescription);
        accountService.createAccount(accountDescription);
        accountService.createAccount(accountDescription);

        accounts = accountService.loadAll();
        assertNotNull("Accounts list is not null");
        assertFalse("Accounts list is not empty", accounts.isEmpty());
    }
}
