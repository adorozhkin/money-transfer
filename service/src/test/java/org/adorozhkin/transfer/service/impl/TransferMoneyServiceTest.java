package org.adorozhkin.transfer.service.impl;

import org.adorozhkin.transfer.dao.AccountDao;
import org.adorozhkin.transfer.model.Account;
import org.adorozhkin.transfer.service.AccountService;
import org.adorozhkin.transfer.service.TransferMoneyService;
import org.adorozhkin.transfer.service.mock.AccountDaoMock;
import org.junit.Test;

public class TransferMoneyServiceTest {
    private static final double DELTA = 0.1;

    private AccountDao accountDao = new AccountDaoMock();
    private AccountService accountService = new AccountServiceImpl(accountDao);
    private TransferMoneyService transferService = new TransferMoneyServiceImpl(accountService);

    @Test(expected = IllegalArgumentException.class)
    public void testTransferMoneyNullFrom() throws Exception {
        transferService.transfer(null, new Account(), DELTA);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTransferMoneyNullTo() throws Exception {
        transferService.transfer(new Account(), null, DELTA);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTransferMoneyNegative() throws Exception {
        transferService.transfer(new Account(), new Account(), -DELTA);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTransferMoneyItself() throws Exception {
        transferService.transfer(new Account(), new Account(), DELTA);
    }

}