package org.adorozhkin.transfer.service.impl;

import org.adorozhkin.transfer.model.Account;
import org.adorozhkin.transfer.service.AccountService;
import org.adorozhkin.transfer.service.TransferMoneyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class TransferMoneyServiceImpl implements TransferMoneyService {
    private static final Logger log = LoggerFactory.getLogger(TransferMoneyServiceImpl.class);

    private final AccountService accountService;

    public TransferMoneyServiceImpl(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public void transfer(Account from, Account to, double amount) {
        if (from == null) {
            throw new IllegalArgumentException("From account shouldn't be null");
        }
        if (to == null) {
            throw new IllegalArgumentException("To account shouldn't be null");
        }
        if (amount < 0) {
            throw new IllegalArgumentException("Amount should be positive");
        }
        if (from.getAccountNumber() == to.getAccountNumber()) {
            throw new IllegalArgumentException("Cannot transfer to itself");
        }

        log.info("Transferring {} from {} to {}", amount, from, to);

        // Sort accounts to prevent deadlocks in concurrent environment
        if (from.compareTo(to) < 0) {
            accountService.withdrawAccount(from, amount);
            accountService.depositAccount(to, amount);
        } else {
            accountService.depositAccount(to, amount);
            accountService.withdrawAccount(from, amount);
        }
    }
}
