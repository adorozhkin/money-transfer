package org.adorozhkin.transfer.service;

import org.adorozhkin.transfer.model.Account;

public interface TransferMoneyService {
    void transfer(Account from, Account to, double amount);
}
