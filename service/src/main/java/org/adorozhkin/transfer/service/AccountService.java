package org.adorozhkin.transfer.service;

import org.adorozhkin.transfer.model.Account;

import java.util.List;

public interface AccountService {
    Account createAccount(String description) throws AccountException;

    Account loadAccount(long accountNumber, boolean forUpdate) throws AccountException;

    List<Account> loadAll() throws AccountException;

    void deleteAccount(Account account) throws AccountException;

    Account depositAccount(Account account, double amount) throws AccountException;

    Account withdrawAccount(Account account, double amount) throws AccountException;
}
