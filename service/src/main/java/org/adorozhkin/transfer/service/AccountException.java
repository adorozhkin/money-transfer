package org.adorozhkin.transfer.service;

public class AccountException extends RuntimeException {
    public AccountException(String message, Throwable cause) {
        super(message, cause);
    }
}
