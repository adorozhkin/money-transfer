package org.adorozhkin.transfer.service.impl;

import org.adorozhkin.transfer.dao.AccountDao;
import org.adorozhkin.transfer.dao.SqlRuntimeException;
import org.adorozhkin.transfer.model.Account;
import org.adorozhkin.transfer.service.AccountException;
import org.adorozhkin.transfer.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

class AccountServiceImpl implements AccountService {
    private final static Logger log = LoggerFactory.getLogger(AccountServiceImpl.class);
    private final AccountDao accountDao;

    public AccountServiceImpl(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    public Account createAccount(String description) {
        if (description == null || description.trim().length() == 0) {
            throw new IllegalArgumentException("Description shouldn't be empty");
        }

        log.info("Creating account '{}'", description);
        try {
            Account account = new Account(description);
            long accountNumber = accountDao.save(account);
            account.setAccountNumber(accountNumber);
            log.info("Account {} is created", account);
            return account;
        } catch (SqlRuntimeException e) {
            throw new AccountException("Failed to create account", e);
        }
    }

    @Override
    public Account loadAccount(long accountNumber, boolean forUpdate) {
        log.info("Loading account {}", accountNumber);
        try {
            Account account = accountDao.load(accountNumber, forUpdate);
            log.info("Account {} is loaded", account);
            return account;
        } catch (SqlRuntimeException e) {
            throw new AccountException("Failed to load account", e);
        }
    }

    @Override
    public List<Account> loadAll() {
        log.info("Loading all accounts");
        try {
            List<Account> accounts = accountDao.loadAll();
            log.info("{} accounts are loaded", accounts.size());
            return accounts;
        } catch (SqlRuntimeException e) {
            throw new AccountException("Failed to load all accounts", e);
        }
    }

    public void deleteAccount(Account account) {
        if (account == null) {
            throw new IllegalArgumentException("Account shouldn't be null");
        }
        log.info("Deleting account {}", account);
        try {
            accountDao.delete(account.getAccountNumber());
            log.info("Account {} deleted", account);
        } catch (SqlRuntimeException e) {
            throw new AccountException("Failed to delete account", e);
        }
    }

    public Account depositAccount(Account account, double amount) {
        checkParams(account, amount);

        log.info("Depositing {} into account {}", amount, account);
        try {
            account.setAmount(account.getAmount() + amount);
            accountDao.update(account);
            log.info("Account {} updated after deposit", account);
            return account;
        } catch (SqlRuntimeException e) {
            throw new AccountException("Failed to deposit account", e);
        }
    }

    public Account withdrawAccount(Account account, double amount) {
        checkParams(account, amount);

        log.info("Withdrawing {} from account {}", amount, account);
        try {
            if (account.getAmount() < amount) {
                throw new IllegalStateException(String.format("Not enough amount on account %s. Need at least %s", account, amount));
            }
            account.setAmount(account.getAmount() - amount);
            accountDao.update(account);
            log.info("Account {} updated after withdraw", account);
            return account;
        } catch (SqlRuntimeException e) {
            throw new AccountException("Failed to withdraw account", e);
        }
    }

    private void checkParams(Account account, double amount) {
        if (account == null) {
            throw new IllegalArgumentException("Account shouldn't be null");
        }
        if (amount < 0) {
            throw new IllegalArgumentException("Amount should be positive");
        }
    }
}
