package org.adorozhkin.transfer.service;

public class TransferException extends RuntimeException {
    public TransferException(String message, Throwable cause) {
        super(message, cause);
    }
}
