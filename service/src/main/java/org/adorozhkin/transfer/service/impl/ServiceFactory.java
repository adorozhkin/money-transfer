package org.adorozhkin.transfer.service.impl;

import org.adorozhkin.transfer.dao.DataSource;
import org.adorozhkin.transfer.dao.impl.AccountDaoImpl;
import org.adorozhkin.transfer.dao.impl.ConnectionManagerImpl;
import org.adorozhkin.transfer.dao.impl.DataSourceImpl;
import org.adorozhkin.transfer.service.AccountService;
import org.adorozhkin.transfer.service.TransferMoneyService;

public class ServiceFactory {
    private DataSource datasource = new DataSourceImpl();
    private ConnectionManagerImpl connectionManager = new ConnectionManagerImpl(datasource);
    private AccountService accountService = new AccountServiceImpl(new AccountDaoImpl(connectionManager));
    private TransferMoneyService transferService = new TransferMoneyServiceImpl(accountService);
    private AccountServiceDecorator serviceDecorator = new AccountServiceDecorator(
            accountService, transferService, connectionManager
    );

    public AccountServiceDecorator getServiceDecorator() {
        return serviceDecorator;
    }
}
