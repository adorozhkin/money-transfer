package org.adorozhkin.transfer.service.impl;

import org.adorozhkin.transfer.dao.ConnectionManager;
import org.adorozhkin.transfer.model.Account;
import org.adorozhkin.transfer.service.AccountNotFoundException;
import org.adorozhkin.transfer.service.AccountService;
import org.adorozhkin.transfer.service.TransferMoneyService;

import java.util.List;

public class AccountServiceDecorator {
    private final AccountService accountService;
    private final TransferMoneyService transferService;
    private final ConnectionManager connectionManager;

    public AccountServiceDecorator(AccountService accountService, TransferMoneyService transferService, ConnectionManager connectionManager) {
        this.accountService = accountService;
        this.transferService = transferService;
        this.connectionManager = connectionManager;
    }

    public List<Account> loadAllAccounts() throws Exception {
        return connectionManager.executeInTransaction(accountService::loadAll);
    }

    public Account loadAccount(long accountNumber) throws Exception {
        return connectionManager.executeInTransaction(() -> accountService.loadAccount(accountNumber, false));
    }

    public void deleteAccount(long accountNumber) throws Exception {
        connectionManager.executeInTransaction(() -> {
            Account account = loadAccount(accountNumber, true);
            accountService.deleteAccount(account);
            return null;
        });
    }

    public Account createAccount(String description) throws Exception {
        return connectionManager.executeInTransaction(() -> accountService.createAccount(description));
    }

    public Account depositAccount(long accountNumber, double amount) throws Exception {
        return connectionManager.executeInTransaction(() -> {
            Account account = loadAccount(accountNumber, true);
            return accountService.depositAccount(account, amount);
        });
    }

    public Account withdrawAccount(long accountNumber, double amount) throws Exception {
        return connectionManager.executeInTransaction(() -> {
            Account account = loadAccount(accountNumber, true);
            return accountService.withdrawAccount(account, amount);
        });
    }

    public Account transfer(long fromAccountNumber, long toAccountNumber, double amount) throws Exception {
        return connectionManager.executeInTransaction(() -> {
            Account[] accounts = new Account[2];
            //reorder to avoid deadlock
            if (fromAccountNumber < toAccountNumber) {
                accounts[0] = loadAccount(fromAccountNumber, true);
                accounts[1] = loadAccount(toAccountNumber, true);
            } else {
                accounts[1] = loadAccount(toAccountNumber, true);
                accounts[0] = loadAccount(fromAccountNumber, true);
            }
            transferService.transfer(accounts[0], accounts[1], amount);
            return accounts[0];
        });
    }

    private Account loadAccount(long accountNumber, boolean forUpdate) throws AccountNotFoundException {
        Account account = accountService.loadAccount(accountNumber, forUpdate);
        if (account == null) {
            throw new AccountNotFoundException("Account not found", accountNumber);
        }
        return account;
    }
}
