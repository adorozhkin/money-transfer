package org.adorozhkin.transfer.service;

public class AccountNotFoundException extends RuntimeException {
    private final long accountNumber;

    public AccountNotFoundException(String message, long accountNumber) {
        super(message);
        this.accountNumber = accountNumber;
    }

    public long getAccountNumber() {
        return accountNumber;
    }
}
