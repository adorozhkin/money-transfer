package org.adorozhkin.transfer.rest.impl;

import com.despegar.http.client.*;
import com.despegar.sparkjava.test.SparkServer;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.adorozhkin.transfer.model.Account;
import org.adorozhkin.transfer.service.impl.ServiceFactory;
import org.junit.ClassRule;
import org.junit.Test;
import spark.servlet.SparkApplication;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class RestControllerTest {
    public static final int PORT = 56789;

    private Gson gson = new Gson();

    private static final String DESCRIPTION = "testAccount1";
    private static final double DELTA = 0.1;
    private static final double AMOUNT = 100.99;

    public static class TestApplication implements SparkApplication {
        private ServiceFactory factory = new ServiceFactory();

        @Override
        public void init() {
            new RestController(factory.getServiceDecorator()).init();
        }
    }


    @ClassRule
    public static SparkServer<TestApplication> testServer = new SparkServer<>(TestApplication.class, PORT);


    @Test
    public void testCreateAccount() throws HttpClientException {
        Account account = createAccount(DESCRIPTION);
        Account loadedAccount = loadAccount(account.getAccountNumber());
        assertTrue("Account number is assigned", loadedAccount.getAccountNumber() > 0);
        assertEquals("Account description is saved", DESCRIPTION, loadedAccount.getDescription());
    }

    @Test
    public void testLoadAll() throws HttpClientException {
        createAccount(DESCRIPTION);
        createAccount(DESCRIPTION + 2);


        GetMethod get = testServer.get("/account", false);
        HttpResponse response = testServer.execute(get);
        assertEquals(200, response.code());
        Type listType = new TypeToken<ArrayList<Account>>() {
        }.getType();

        List<Account> loadedAccounts = gson.fromJson(new String(response.body()), listType);
        assertNotNull("Accounts list is not null", loadedAccounts);
        assertFalse("Accounts list is not empty", loadedAccounts.isEmpty());
    }

    @Test
    public void testDeleteAccount() throws HttpClientException {
        Account account = createAccount(DESCRIPTION);

        DeleteMethod delete = testServer.delete("/account/" + account.getAccountNumber(), false);
        HttpResponse response = testServer.execute(delete);
        assertEquals(200, response.code());

        GetMethod get = testServer.get("/account/" + account.getAccountNumber(), false);
        response = testServer.execute(get);
        assertEquals(404, response.code());
    }

    @Test
    public void testDeleteAccountNotFound() throws HttpClientException {
        DeleteMethod delete = testServer.delete("/account/" + -1, false);
        HttpResponse response = testServer.execute(delete);
        assertEquals(404, response.code());
        assertTrue("Error message is present", new String(response.body()).contains("not found"));
    }

    @Test
    public void testDepositAccount() throws HttpClientException {
        Account account = createAccount(DESCRIPTION);

        HttpResponse response = modifyRequest(account, "deposit", AMOUNT, 200);
        Account loadedAccount = gson.fromJson(new String(response.body()), Account.class);
        assertNotNull("Account exists", loadedAccount);
        assertEquals("Amount after deposit", AMOUNT, loadedAccount.getAmount(), DELTA);

        loadedAccount = loadAccount(account.getAccountNumber());
        assertNotNull("Account exists", loadedAccount);
        assertEquals("Amount after deposit", AMOUNT, loadedAccount.getAmount(), DELTA);
    }

    @Test
    public void testWithdrawAccount() throws HttpClientException {
        Account account = createAccount(DESCRIPTION);

        HttpResponse response = modifyRequest(account, "deposit", AMOUNT, 200);
        Account loadedAccount = gson.fromJson(new String(response.body()), Account.class);
        assertEquals("Amount after deposit", AMOUNT, loadedAccount.getAmount(), DELTA);

        response = modifyRequest(account, "withdraw", AMOUNT / 3, 200);
        loadedAccount = gson.fromJson(new String(response.body()), Account.class);
        assertEquals("Amount after deposit", AMOUNT * 2 / 3, loadedAccount.getAmount(), DELTA);

        loadedAccount = loadAccount(account.getAccountNumber());
        assertNotNull("Account exists", loadedAccount);
        assertEquals("Amount after deposit", AMOUNT * 2 / 3, loadedAccount.getAmount(), DELTA);
    }

    @Test
    public void testDepositNegative() throws HttpClientException {
        Account account = createAccount(DESCRIPTION);
        HttpResponse response = modifyRequest(account, "deposit", -AMOUNT, 400);
        assertTrue("Error message is present", new String(response.body()).contains("Amount should be positive"));
    }

    @Test
    public void testWithdrawNegative() throws HttpClientException {
        Account account = createAccount(DESCRIPTION);
        HttpResponse response = modifyRequest(account, "withdraw", -AMOUNT, 400);
        assertTrue("Error message is present", new String(response.body()).contains("Amount should be positive"));
    }

    @Test
    public void testDepositNotFound() throws HttpClientException {
        Account account = new Account();
        account.setAccountNumber(-1);
        HttpResponse response = modifyRequest(account, "deposit", AMOUNT, 404);
        assertTrue("Error message is present", new String(response.body()).contains("not found"));
    }

    @Test
    public void testWithdrawNotFound() throws HttpClientException {
        Account account = new Account();
        account.setAccountNumber(-1);
        HttpResponse response = modifyRequest(account, "withdraw", AMOUNT, 404);
        assertTrue("Error message is present", new String(response.body()).contains("not found"));
    }

    @Test
    public void testCreateAccountInvalidRequest() throws HttpClientException {
        PostMethod post = testServer.post("/account", "bad request", false);
        HttpResponse response = testServer.execute(post);
        assertEquals(500, response.code());
    }

    @Test
    public void testInvalidLoad() throws HttpClientException {
        GetMethod get = testServer.get("/account/bad", false);
        HttpResponse response = testServer.execute(get);
        assertEquals(400, response.code());
        assertTrue("Error message is present", new String(response.body()).contains("Invalid account number"));
    }

    @Test
    public void testTransfer() throws HttpClientException {
        Account account1 = createAccount(DESCRIPTION);
        Account account2 = createAccount(DESCRIPTION);

        modifyRequest(account1, "deposit", AMOUNT, 200);
        TransferMoneyRequest transferRequest = new TransferMoneyRequest(account1.getAccountNumber(), account2.getAccountNumber(), AMOUNT);
        PostMethod put = testServer.post("/transfer", gson.toJson(transferRequest), false);
        HttpResponse response = testServer.execute(put);
        assertEquals(200, response.code());

        Account loadedAccount1 = loadAccount(account1.getAccountNumber());
        Account loadedAccount2 = loadAccount(account2.getAccountNumber());
        assertEquals("Account 1 withdrawn", 0, loadedAccount1.getAmount(), DELTA);
        assertEquals("Account 2 deposited", AMOUNT, loadedAccount2.getAmount(), DELTA);
    }

    @Test
    public void testTransferInvalidRequest() throws HttpClientException {
        PostMethod post = testServer.post("/transfer", "bad request", false);
        HttpResponse response = testServer.execute(post);
        assertEquals(500, response.code());
    }

    @Test
    public void testTransferNotEnough() throws HttpClientException {
        Account account1 = createAccount(DESCRIPTION);
        Account account2 = createAccount(DESCRIPTION);

        TransferMoneyRequest transferRequest = new TransferMoneyRequest(account1.getAccountNumber(), account2.getAccountNumber(), AMOUNT);
        PostMethod put = testServer.post("/transfer", gson.toJson(transferRequest), false);
        HttpResponse response = testServer.execute(put);
        assertEquals(500, response.code());
        assertTrue("Error message is present", new String(response.body()).contains("Not enough amount"));
    }

    @Test
    public void testTransferNotFound() throws HttpClientException {
        TransferMoneyRequest transferRequest = new TransferMoneyRequest(-1, -2, AMOUNT);
        PostMethod put = testServer.post("/transfer", gson.toJson(transferRequest), false);
        HttpResponse response = testServer.execute(put);
        assertEquals(404, response.code());
        assertTrue("Error message is present", new String(response.body()).contains("not found"));
    }

    @Test
    public void testTransferNegative() throws HttpClientException {
        Account account1 = createAccount(DESCRIPTION);
        Account account2 = createAccount(DESCRIPTION);

        modifyRequest(account1, "deposit", AMOUNT, 200);
        TransferMoneyRequest transferRequest = new TransferMoneyRequest(account1.getAccountNumber(), account2.getAccountNumber(), -AMOUNT);
        PostMethod put = testServer.post("/transfer", gson.toJson(transferRequest), false);
        HttpResponse response = testServer.execute(put);
        assertEquals(400, response.code());
        assertTrue("Error message is present", new String(response.body()).contains("Amount should be positive"));
    }


    private Account createAccount(String description) throws HttpClientException {
        CreateAccountRequest request = new CreateAccountRequest(description);
        PostMethod post = testServer.post("/account", gson.toJson(request), false);

        HttpResponse response = testServer.execute(post);
        assertEquals(200, response.code());
        Account account = gson.fromJson(new String(response.body()), Account.class);
        assertTrue("Account number is assigned", account.getAccountNumber() > 0);
        assertEquals("Account description is saved", description, account.getDescription());
        assertTrue("Content-Type", response.headers().get("Content-Type").contains("application/json"));
        return account;
    }

    private Account loadAccount(long accountNumber) throws HttpClientException {
        GetMethod get = testServer.get("/account/" + accountNumber, false);
        HttpResponse response = testServer.execute(get);
        assertEquals(200, response.code());
        assertTrue("Content-Type", response.headers().get("Content-Type").contains("application/json"));
        return gson.fromJson(new String(response.body()), Account.class);
    }

    private HttpResponse modifyRequest(Account account, final String path, double amount, int status) throws HttpClientException {
        ModifyAccountRequest modifyRequest = new ModifyAccountRequest(account.getAccountNumber(), amount);
        PutMethod put = testServer.put("/account/" + path, gson.toJson(modifyRequest), false);
        HttpResponse response = testServer.execute(put);
        assertEquals(status, response.code());
        assertTrue("Content-Type", response.headers().get("Content-Type").contains("application/json"));
        return response;
    }

}