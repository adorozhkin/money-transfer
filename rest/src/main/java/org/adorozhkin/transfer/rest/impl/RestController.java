package org.adorozhkin.transfer.rest.impl;

import com.google.gson.Gson;
import org.adorozhkin.transfer.model.Account;
import org.adorozhkin.transfer.service.AccountNotFoundException;
import org.adorozhkin.transfer.service.TransferException;
import org.adorozhkin.transfer.service.impl.AccountServiceDecorator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.HaltException;
import spark.Request;
import spark.Response;
import spark.ResponseTransformer;

import java.util.List;

import static spark.Spark.*;

public class RestController {
    private static final Logger log = LoggerFactory.getLogger(RestController.class);

    private final AccountServiceDecorator accountService;
    private final Gson gson = new Gson();
    private final ResponseTransformer jsonRenderer = gson::toJson;

    public RestController(AccountServiceDecorator accountService) {
        this.accountService = accountService;
    }

    public RestController init() {
        setupEndpoints();
        return this;
    }

    private void setupEndpoints() {
        log.info("Setting up endpoints");

        get("/account", this::loadAllAccounts, jsonRenderer);
        get("/account/:accountNumber", this::loadAccount, jsonRenderer);
        delete("/account/:accountNumber", this::deleteAccount);
        post("/account", this::createAccount, jsonRenderer);
        put("/account/deposit", this::depositAccount, jsonRenderer);
        put("/account/withdraw", this::withdrawAccount, jsonRenderer);
        post("/transfer", this::transferMoney, jsonRenderer);

        before((request, response) -> response.type("application/json"));

        log.info("Endpoints setup is done");
    }


    private List<Account> loadAllAccounts(Request req, Response res) throws Exception {
        try {
            log.info("Load all accounts is requested");
            List<Account> accounts = accountService.loadAllAccounts();
            log.info("All accounts are loaded");
            return accounts;
        } catch (Exception e) {
            throw halt(500, e.getMessage());
        }
    }

    private Account loadAccount(Request req, Response res) throws Exception {
        String accountNumberParam = req.params(":accountNumber");
        log.info("Load account with account number '{}'", accountNumberParam);
        try {
            long accountNumber = Long.parseLong(accountNumberParam);
            Account account = accountService.loadAccount(accountNumber);
            if (account == null) {
                throw halt(404, String.format("Account with number %s is not found", accountNumberParam));
            }
            log.info("Account with account number '{}' is loaded", accountNumber);
            return account;
        } catch (NumberFormatException e) {
            String msg = String.format("Invalid account number '%s' provided", accountNumberParam);
            log.error(msg, accountNumberParam, e);
            throw halt(400, msg);
        } catch (HaltException e) {
            throw e;
        } catch (Exception e) {
            throw halt(500, e.getMessage());
        }
    }

    private String deleteAccount(Request req, Response res) throws Exception {
        String accountNumberParam = req.params(":accountNumber");
        try {
            log.info("Delete account with account number '{}'", accountNumberParam);
            long accountNumber = Long.parseLong(accountNumberParam);
            accountService.deleteAccount(accountNumber);
            log.info("Account with account number '{}' is removed", accountNumber);
            return "OK";
        } catch (NumberFormatException e) {
            String msg = String.format("Invalid account number '%s' provided", accountNumberParam);
            log.error(msg, accountNumberParam, e);
            throw halt(400, msg);
        } catch (AccountNotFoundException e) {
            throw halt(404, String.format("Account with number %s is not found", e.getAccountNumber()));
        } catch (Exception e) {
            throw halt(500, e.getMessage());
        }
    }

    private Account createAccount(Request req, Response res) throws Exception {
        try {
            log.info("Creating account");
            CreateAccountRequest account = gson.fromJson(req.body(), CreateAccountRequest.class);
            log.info("Create account params {}", account);
            Account response = accountService.createAccount(account.getDescription());
            log.info("Account {} is created", response);
            return response;
        } catch (Exception e) {
            throw halt(500, e.getMessage());
        }
    }

    private Account transferMoney(Request req, Response res) throws Exception {
        long fromAccountNumber = -1;
        long toAccountNumber = -1;
        double amount = -1;
        try {
            log.info("Transferring money");
            TransferMoneyRequest transferRequest = gson.fromJson(req.body(), TransferMoneyRequest.class);
            log.info("Transfer money params {}", transferRequest);
            fromAccountNumber = transferRequest.getFrom();
            toAccountNumber = transferRequest.getTo();
            amount = transferRequest.getAmount();

            checkAmount(transferRequest.getAmount());
            Account from = accountService.transfer(fromAccountNumber, toAccountNumber, amount);
            log.info("Money transferred");
            return from;
        } catch (HaltException e) {
            throw e;
        } catch (AccountNotFoundException e) {
            throw halt(404, String.format("Account with number %s is not found", e.getAccountNumber()));
        } catch (TransferException e) {
            String msg = String.format("Failed to transfer %s from %s to %s", amount, fromAccountNumber, toAccountNumber);
            log.error(msg, e);
            throw halt(500, msg);
        } catch (Exception e) {
            throw halt(500, e.getMessage());
        }
    }

    private Account depositAccount(Request req, Response res) throws Exception {
        log.info("Deposit account");
        try {
            ModifyAccountRequest modifyRequest = gson.fromJson(req.body(), ModifyAccountRequest.class);
            log.info("Deposit account params {}", modifyRequest);
            checkAmount(modifyRequest.getAmount());
            Account result = accountService.depositAccount(modifyRequest.getAccountNumber(), modifyRequest.getAmount());
            log.info("Account {} deposited", result);
            return result;
        } catch (HaltException e) {
            throw e;
        } catch (AccountNotFoundException e) {
            throw halt(404, String.format("Account with number %s is not found", e.getAccountNumber()));
        } catch (Exception e) {
            throw halt(500, e.getMessage());
        }
    }

    private Account withdrawAccount(Request req, Response res) throws Exception {
        log.info("Withdraw account");
        try {
            ModifyAccountRequest modifyRequest = gson.fromJson(req.body(), ModifyAccountRequest.class);
            log.info("Withdraw account params {}", modifyRequest);
            checkAmount(modifyRequest.getAmount());
            Account result = accountService.withdrawAccount(modifyRequest.getAccountNumber(), modifyRequest.getAmount());
            log.info("Account {} withdrawn", result);
            return result;
        } catch (HaltException e) {
            throw e;
        } catch (AccountNotFoundException e) {
            throw halt(404, String.format("Account with number %s is not found", e.getAccountNumber()));
        } catch (Exception e) {
            throw halt(500, e.getMessage());
        }
    }

    private void checkAmount(double amount) {
        if (amount <= 0) {
            throw halt(400, "Amount should be positive");
        }
    }

}
