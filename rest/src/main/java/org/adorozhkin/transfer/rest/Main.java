package org.adorozhkin.transfer.rest;

import org.adorozhkin.transfer.rest.impl.RestController;
import org.adorozhkin.transfer.service.impl.ServiceFactory;

import java.io.IOException;
import java.util.Properties;

import static spark.Spark.*;

public class Main {
    public static void main(String[] args) {
        int port = 8080;
        if (args.length > 0) {
            port = parseInt(args[0], "Invalid port provided. Using default port " + port, port);
        }
        new Main().startServer(port);
    }

    private static int parseInt(String str, String msg, int defaultValue) {
        try {
            return Integer.parseInt(str);
        } catch (NumberFormatException e) {
            System.out.println(msg);
            System.out.println(e.getMessage());
            return defaultValue;
        }
    }

    private void startServer(int port) {
        ServiceFactory factory = new ServiceFactory();

        int maxThreads = 10;
        int minThreads = 1;
        int idleTimeout = 30_000;
        Properties serverProperties = new Properties();
        try {
            serverProperties.load(getClass().getClassLoader().getResourceAsStream("rest.properties"));
            maxThreads = parseInt(serverProperties.getProperty("max.threads"), "Invalid max.threads provided.", maxThreads);
            minThreads = parseInt(serverProperties.getProperty("min.threads"), "Invalid min.threads provided.", minThreads);
            idleTimeout = parseInt(serverProperties.getProperty("idle.timeout"), "Invalid idle.timeout provided. ", idleTimeout);
        } catch (IOException e) {
            System.out.println("Failed to load server.properties from classpath. Using default settings");
        }

        port(port);
        threadPool(maxThreads, minThreads, idleTimeout);

        options("/*", (req, res) -> {
            String accessControlRequestHeaders = req.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                res.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }

            String accessControlRequestMethod = req.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                res.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }

            return "OK";
        });

        before((request, response) -> response.header("Access-Control-Allow-Origin", "*"));
        before((request, response) -> response.header("Access-Control-Allow-Headers", "*"));

        new RestController(factory.getServiceDecorator()).init();
    }
}
