package org.adorozhkin.transfer.rest.impl;

public class ModifyAccountRequest {
    private long accountNumber;
    private double amount;

    public ModifyAccountRequest() {
    }

    public ModifyAccountRequest(long accountNumber, double amount) {
        this.accountNumber = accountNumber;
        this.amount = amount;
    }

    public long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "ModifyAccountRequest{" +
                "accountNumber=" + accountNumber +
                ", amount=" + amount +
                '}';
    }
}
