package org.adorozhkin.transfer.rest.impl;

public class CreateAccountRequest {
    private String description;

    public CreateAccountRequest() {
    }

    public CreateAccountRequest(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "CreateAccountRequest{" +
                "description='" + description + '\'' +
                '}';
    }
}
