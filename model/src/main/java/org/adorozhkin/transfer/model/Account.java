package org.adorozhkin.transfer.model;

public class Account implements Comparable<Account> {
    private long accountNumber;
    private String description;
    private double amount;

    public Account() {
    }

    public Account(String description) {
        this.description = description;
    }

    public long getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(long accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        if (accountNumber != account.accountNumber) return false;
        if (Double.compare(account.amount, amount) != 0) return false;
        return description != null ? description.equals(account.description) : account.description == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = (int) (accountNumber ^ (accountNumber >>> 32));
        result = 31 * result + (description != null ? description.hashCode() : 0);
        temp = Double.doubleToLongBits(amount);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountNumber=" + accountNumber +
                ", description='" + description + '\'' +
                ", amount=" + amount +
                '}';
    }

    public int compareTo(Account o) {
        return Long.compare(accountNumber, o.accountNumber);
    }
}
