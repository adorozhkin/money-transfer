# MONEY TRANSFER CODING TASK#

## DESCRIPTION ##
Simple REST interface to transfer money between accounts.

This project can be build with Java 1.8 and above using Maven.

No spring or other heavy frameworks are used per requirements

  - **Logback** for logging

  - **Gson** for JSON marshalling/unmarshalling

  - **SparkJava** for HTTP

  - **Hikari** as database pool

  - **H2** as in-memory database

## BUILD ##
To build run following command
```
mvn clean package
```

it will build uber jar in rest\target\ then you can run server via
```
java -jar rest\target\rest-0.0.1-SNAPSHOT.jar [port]
```

default port is 8080

To run stress test use
```
mvn clean package -P stressTest
```

## REST API ##
Once project is build you can use api-sampler/api.html to try REST API
following endpoints are exposed:

  - **[GET] /account** - loads all accounts from database

  - **[GET] /account/:accountNumber** - loads account for provided account number

  - **[DELETE] /account/:accountNumber** - removes account from database

  - **[POST] /account** - creates account. Accept json object in following format:
   ```json
    {
     "description" : "account description"
    }
   ```

  - **[PUT] /account/deposit** - deposit money into account. Accept json object in following format:
   ```json
    {
     "accountNumber" : "account number to deposit",
     "amount": [amount of money to deposit]
   }
   ```

  - **[PUT] /account/withdraw** - withdraw money from account. Accept json object in following format:
   ```json
   {
    "accountNumber" : "account number to withdraw",
    "amount": [amount of money to withdraw]
   }
   ```

  - **[POST] /transfer** - transfer money from one account into another. Accept json object in following format:
   ```json
   {
    "from" : "account number to withdraw",
    "to" : "account number to deposit",
    "amount": [amount of money to transfer ]
   }
   ```