package org.adorozhkin.transfer.dao;

import org.adorozhkin.transfer.dao.impl.AccountDaoImpl;
import org.adorozhkin.transfer.dao.impl.ConnectionManagerImpl;
import org.adorozhkin.transfer.dao.impl.DataSourceImpl;
import org.adorozhkin.transfer.model.Account;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class AccountDaoTest {
    private DataSource datasource = new DataSourceImpl();
    private ConnectionManager connectionManager = new ConnectionManagerImpl(datasource);
    private AccountDao accountDao = new AccountDaoImpl(connectionManager);

    private static final String DESCRIPTION = "test";

    @Test
    public void testSaveAccount() throws Exception {
        long accountNumber =
                connectionManager.executeInTransaction(() -> accountDao.save(new Account(DESCRIPTION)));
        assertTrue("Account number is generated", accountNumber > 0);
    }

    @Test
    public void testLoadAccount() throws Exception {
        Account loadedAccount =
                connectionManager.executeInTransaction(() -> {
                    long accountNumber = accountDao.save(new Account(DESCRIPTION));
                    return accountDao.load(accountNumber, false);
                });
        assertNotNull("Account is loaded", loadedAccount);
        assertEquals("Account description is the same", DESCRIPTION, loadedAccount.getDescription());
    }

    @Test
    public void testDeleteAccount() throws Exception {
        Account account = connectionManager.executeInTransaction(() -> {
            long accountNumber = accountDao.save(new Account(DESCRIPTION));

            Account loadedAccount = accountDao.load(accountNumber, true);
            assertNotNull("Account is loaded", loadedAccount);
            accountDao.delete(accountNumber);
            return accountDao.load(accountNumber, false);
        });
        assertNull("Account is removed", account);
    }

    @Test
    public void testLoadAll() throws Exception {
        List<Account> accounts = connectionManager.executeInTransaction(() -> {
            accountDao.save(new Account(DESCRIPTION));
            accountDao.save(new Account(DESCRIPTION));
            accountDao.save(new Account(DESCRIPTION));
            return accountDao.loadAll();
        });
        assertNotNull("Accounts list is not null", accounts);
        assertFalse("Account is not empty", accounts.isEmpty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void saveNullAccount() {
        accountDao.save(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateNullAccount() {
        accountDao.update(null);
    }
}