package org.adorozhkin.transfer.dao;

import org.adorozhkin.transfer.model.Account;

import java.util.List;

public interface AccountDao {
    long save(Account account);

    void update(Account account);

    void delete(long accountNumber);

    Account load(long accountNumber, boolean forUpdate);

    List<Account> loadAll();

}
