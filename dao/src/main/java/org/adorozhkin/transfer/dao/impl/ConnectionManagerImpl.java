package org.adorozhkin.transfer.dao.impl;

import org.adorozhkin.transfer.dao.ConnectionManager;
import org.adorozhkin.transfer.dao.DataSource;
import org.adorozhkin.transfer.dao.SqlRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class ConnectionManagerImpl implements ConnectionManager {
    private static final Logger log = LoggerFactory.getLogger(ConnectionManagerImpl.class);
    private ThreadLocal<Connection> connectionHolder = new ThreadLocal<>();
    private final DataSource datasource;

    public ConnectionManagerImpl(DataSource datasource) {
        this.datasource = datasource;
    }

    @Override
    public long executeInsert(String sql, Consumer<PreparedStatement> paramsSetter) {
        PreparedStatement pst = null;
        try {
            pst = getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            paramsSetter.accept(pst);
            pst.executeUpdate();
            ResultSet keys = pst.getGeneratedKeys();
            return keys.next() ? keys.getLong(1) : -1;
        } catch (SQLException e) {
            throw new SqlRuntimeException(e);
        } finally {
            closeResource(pst);
        }
    }

    @Override
    public void executeUpdate(String sql, Consumer<PreparedStatement> paramsSetter) {
        PreparedStatement pst = null;
        try {
            pst = getConnection().prepareStatement(sql);
            paramsSetter.accept(pst);
            pst.executeUpdate();
        } catch (SQLException e) {
            throw new SqlRuntimeException(e);
        } finally {
            closeResource(pst);
        }
    }

    @Override
    public <T> T executeQuery(String sql, Consumer<PreparedStatement> paramsSetter, Function<ResultSet, T> resultMapper) {
        ResultSet rs = null;
        PreparedStatement pst = null;
        try {
            pst = getConnection().prepareStatement(sql);
            paramsSetter.accept(pst);
            rs = pst.executeQuery();
            return resultMapper.apply(rs);
        } catch (SQLException e) {
            throw new SqlRuntimeException(e);
        } finally {
            closeResource(rs);
            closeResource(pst);
        }
    }

    @Override
    public <T> T executeInTransaction(Supplier<T> supplier) throws Exception {
        try {
            beginTransaction();
            T result = supplier.get();
            commitTransaction();
            return result;
        } catch (Exception e) {
            try {
                rollbackTransaction();
            } catch (SQLException sqle) {
                log.error("Rollback failed", sqle);
            }
            throw e;
        }
    }

    private void beginTransaction() throws SQLException {
        createConnection();
    }

    private void commitTransaction() throws SQLException {
        log.debug("Committing transaction");
        getConnection().commit();
        closeConnection();
    }

    private void rollbackTransaction() throws SQLException {
        log.debug("Rolling back transaction");
        getConnection().rollback();
        closeConnection();
    }

    private Connection getConnection() throws SQLException {
        Connection connection = connectionHolder.get();
        if (connection == null) {
            throw new IllegalStateException("Not in transaction");
        }
        return connection;
    }

    private void closeConnection() throws SQLException {
        getConnection().setAutoCommit(true);
        getConnection().close();
        connectionHolder.remove();
    }

    private Connection createConnection() throws SQLException {
        log.debug("Creating connection");
        if (connectionHolder.get() != null) {
            throw new IllegalStateException("Connection already exists for current thread");
        }
        Connection connection = datasource.getConnection();
        connection.setAutoCommit(false);
        connectionHolder.set(connection);
        return connection;
    }

    private void closeResource(AutoCloseable pst) {
        if (pst != null) {
            try {
                pst.close();
            } catch (Exception e) {
                log.error("Failed to close prepared statement", e);
            }
        }
    }
}
