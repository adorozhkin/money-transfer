package org.adorozhkin.transfer.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public interface ConnectionManager {

    long executeInsert(String sql, Consumer<PreparedStatement> paramsSetter);

    void executeUpdate(String sql, Consumer<PreparedStatement> paramsSetter);

    <T> T executeQuery(String sql, Consumer<PreparedStatement> paramsSetter, Function<ResultSet, T> resultMapper);

    <T> T executeInTransaction(Supplier<T> supplier) throws Exception;

}
