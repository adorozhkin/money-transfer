package org.adorozhkin.transfer.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.Callable;

public class SqlUtils {
    private SqlUtils() {
    }

    public static void setLong(PreparedStatement pst, int index, long value) {
        hideSqlException(() -> {
            pst.setLong(index, value);
            return null;
        });
    }

    public static void setString(PreparedStatement pst, int index, String value) {
        hideSqlException(() -> {
            pst.setString(index, value);
            return null;
        });
    }

    public static void setDouble(PreparedStatement pst, int index, double value) {
        hideSqlException(() -> {
            pst.setDouble(index, value);
            return null;
        });
    }

    public static long getLong(ResultSet rs, int index) {
        return hideSqlException(() -> rs.getLong(index));
    }

    public static String getString(ResultSet rs, int index) {
        return hideSqlException(() -> rs.getString(index));
    }

    public static double getDouble(ResultSet rs, int index) {
        return hideSqlException(() -> rs.getDouble(index));
    }

    public static boolean next(ResultSet rs) {
        return hideSqlException(() -> rs.next());
    }

    private static <T> T hideSqlException(Callable<T> supplier) {
        try {
            return supplier.call();
        } catch (SQLException e) {
            throw new SqlRuntimeException(e);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
