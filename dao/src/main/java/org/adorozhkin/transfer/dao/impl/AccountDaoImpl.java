package org.adorozhkin.transfer.dao.impl;

import org.adorozhkin.transfer.dao.AccountDao;
import org.adorozhkin.transfer.dao.ConnectionManager;
import org.adorozhkin.transfer.dao.SqlUtils;
import org.adorozhkin.transfer.model.Account;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class AccountDaoImpl implements AccountDao {
    private final ConnectionManager connectionManager;

    public AccountDaoImpl(ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    public long save(Account account) {
        checkAccount(account);
        return connectionManager.executeInsert("INSERT INTO ACCOUNT(ACCOUNT_NUMBER, DESCRIPTION, AMOUNT) VALUES (ACCOUNT_SEQ.NEXTVAL,?,?)",
                pst -> {
                    SqlUtils.setString(pst, 1, account.getDescription());
                    SqlUtils.setDouble(pst, 2, account.getAmount());
                });
    }


    public void update(Account account) {
        checkAccount(account);
        connectionManager.executeUpdate("UPDATE ACCOUNT SET DESCRIPTION=?, AMOUNT=? WHERE ACCOUNT_NUMBER=?",
                pst -> {
                    SqlUtils.setString(pst, 1, account.getDescription());
                    SqlUtils.setDouble(pst, 2, account.getAmount());
                    SqlUtils.setLong(pst, 3, account.getAccountNumber());
                });

    }

    public void delete(long accountNumber) {
        connectionManager.executeUpdate("DELETE FROM ACCOUNT WHERE ACCOUNT_NUMBER=?",
                pst -> {
                    SqlUtils.setLong(pst, 1, accountNumber);
                });
    }

    public Account load(long accountNumber, boolean forUpdate) {
        return connectionManager.executeQuery("SELECT ACCOUNT_NUMBER, DESCRIPTION, AMOUNT FROM ACCOUNT" +
                        " WHERE ACCOUNT_NUMBER=?" +
                        (forUpdate ? " FOR UPDATE " : ""),
                pst -> SqlUtils.setLong(pst, 1, accountNumber),
                (ResultSet rs) -> SqlUtils.next(rs) ? mapAccount(rs) : null);
    }

    @Override
    public List<Account> loadAll() {
        return connectionManager.executeQuery("SELECT ACCOUNT_NUMBER, DESCRIPTION, AMOUNT FROM ACCOUNT",
                pst -> {
                },
                (ResultSet rs) -> {
                    List<Account> accounts = new ArrayList<>();
                    while (SqlUtils.next(rs)) {
                        accounts.add(mapAccount(rs));
                    }
                    return accounts;
                });
    }

    private void checkAccount(Account account) {
        if (account == null) {
            throw new IllegalArgumentException("Account shouldn't be null");
        }
    }

    private Account mapAccount(ResultSet rs) {
        Account account;
        account = new Account();
        account.setAccountNumber(SqlUtils.getLong(rs, 1));
        account.setDescription(SqlUtils.getString(rs, 2));
        account.setAmount(SqlUtils.getDouble(rs, 3));
        return account;
    }
}
