package org.adorozhkin.transfer.dao.impl;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.adorozhkin.transfer.dao.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class DataSourceImpl implements DataSource {
    private static final Logger log = LoggerFactory.getLogger(DataSourceImpl.class);

    private HikariConfig config = new HikariConfig();
    private HikariDataSource ds;
    private Properties databaseProperties = new Properties();

    public DataSourceImpl() {
        try {
            databaseProperties.load(getClass().getClassLoader().getResourceAsStream("database.properties"));
        } catch (IOException e) {
            log.error("Failed to load database.properties from classpath");
        }
        config.setJdbcUrl(databaseProperties.getProperty("database.url"));
        config.setUsername(databaseProperties.getProperty("database.user"));
        config.setPassword(databaseProperties.getProperty("database.password"));
        int poolSize = 10;
        try {
            poolSize = Integer.parseInt(databaseProperties.getProperty("database.max.pool.size"));
        } catch (NumberFormatException e) {
            log.error("Failed to parse database pool size", e);
        }
        config.setMaximumPoolSize(poolSize);
        ds = new HikariDataSource(config);
    }

    public Connection getConnection() throws SQLException {
        return ds.getConnection();
    }
}
