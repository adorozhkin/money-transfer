package org.adorozhkin.transfer.dao;

import java.sql.SQLException;

public class SqlRuntimeException extends RuntimeException {
    private SQLException sqlException;

    public SqlRuntimeException(String message, SQLException cause) {
        super(message, cause);
        this.sqlException = cause;
    }

    public SqlRuntimeException(SQLException cause) {
        super(cause);
        this.sqlException = cause;
    }

    public SQLException getSqlException() {
        return sqlException;
    }
}
