function loadAll(){
$.ajax({
    url: 'http://localhost:8080/account',
    success: function(data){
        $('.resultingDiv').hide()
        $('#allAccountsResult').show()
        $('#allAccountsResult').text(JSON.stringify(data));
    },
    error: function(jqXHR, exception) {
        $('.resultingDiv').hide()
        $('#allAccountsResult').show()
        $('#allAccountsResult').text(jqXHR.status+' '+jqXHR.responseText);
    }
});
}

function createAccount(accountDescription){
var createRequest={};
createRequest.description = accountDescription;
$.ajax({
    url: 'http://localhost:8080/account',
    method: 'POST',
    data: JSON.stringify(createRequest),
    success: function(data){
        $('.resultingDiv').hide()
        $('#createAccountsResult').show()
        $('#createAccountsResult').text(JSON.stringify(data));
        },
    error: function(jqXHR, exception) {
        $('.resultingDiv').hide()
        $('#createAccountsResult').show()
        $('#createAccountsResult').text(jqXHR.status+' '+jqXHR.responseText);
    }
});
}

function loadAccount(accountNumber){
$.ajax({
    url: 'http://localhost:8080/account/'+accountNumber,
    success: function(data){
        $('.resultingDiv').hide()
        $('#loadAccountsResult').show()
        $('#loadAccountsResult').text(JSON.stringify(data));
    },
    error: function(jqXHR, exception) {
        $('.resultingDiv').hide()
        $('#loadAccountsResult').show()
        $('#loadAccountsResult').text(jqXHR.status+' '+jqXHR.responseText);
    }
    });
}

function deleteAccount(accountNumber){
$.ajax({
    url: 'http://localhost:8080/account/'+accountNumber,
    method: 'DELETE',
    success: function(data){
        $('.resultingDiv').hide()
        $('#deleteAccountsResult').show()
        $('#deleteAccountsResult').text(data);
    },
    error: function(jqXHR, exception) {
        $('.resultingDiv').hide()
        $('#deleteAccountsResult').show()
        $('#deleteAccountsResult').text(jqXHR.status+' '+jqXHR.responseText);
    }
    });
}

function depositAccount(accountNumber, amount){
var depositRequest={};
depositRequest.accountNumber=accountNumber;
depositRequest.amount=amount;
$.ajax({
    url: 'http://localhost:8080/account/deposit',
    method: 'PUT',
    data: JSON.stringify(depositRequest),
    success: function(data){
        $('.resultingDiv').hide()
        $('#depositAccountsResult').show()
        $('#depositAccountsResult').text(JSON.stringify(data));
    },
    error: function(jqXHR, exception) {
        $('.resultingDiv').hide()
        $('#depositAccountsResult').show()
        $('#depositAccountsResult').text(jqXHR.status+' '+jqXHR.responseText);
    }
    });
}

function withdrawAccount(accountNumber, amount){
var withdrawRequest={};
withdrawRequest.accountNumber=accountNumber;
withdrawRequest.amount=amount;

$.ajax({
    url: 'http://localhost:8080/account/withdraw',
    method: 'PUT',
    data: JSON.stringify(withdrawRequest),
    success: function(data){
        $('.resultingDiv').hide()
        $('#withdrawAccountsResult').show()
        $('#withdrawAccountsResult').text(JSON.stringify(data));
    },
    error: function(jqXHR, exception) {
        $('.resultingDiv').hide()
        $('#withdrawAccountsResult').show()
        $('#withdrawAccountsResult').text(jqXHR.status+' '+jqXHR.responseText);
    }
    });
}

function transferMoney(from, to, amount){
var transferRequest={};
transferRequest.from=from;
transferRequest.to=to;
transferRequest.amount=amount;

$.ajax({
    url: 'http://localhost:8080/transfer',
    method: 'POST',
    data: JSON.stringify(transferRequest),
    success: function(data){
        $('.resultingDiv').hide()
        $('#transferResult').show()
        $('#transferResult').text(JSON.stringify(data));
    },
    error: function(jqXHR, exception) {
        $('.resultingDiv').hide()
        $('#transferResult').show()
        $('#transferResult').text(jqXHR.status+' '+jqXHR.responseText);
    }
    });
}